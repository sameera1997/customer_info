<?php

$conn = new mysqli("localhost", "root", "", "info");

$query = "SELECT * FROM `user`";

$result = mysqli_query($conn, $query);

?>
<!DOCTYPE html>
<html>

<head>
    <title>User Info</title>
    <link rel="stylesheet" type="text/css" href="infoCss.css" />
    <script type="text/JavaScript" src="infoJS.js">  </script>
</head>

<body>
    <section>
        <div class="display-Form">

            <h2>Customer Details</h2>

            <table>
                <tr>
                    <th>Sl No</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Date of Birth</th>
                    <th>Country Of Birth </th>
                    <th> Driver Licensce </th>
                    <th> Driver Licensce Date </th>
                    <th>Driver Image </th>
                </tr>

               
                <?php 
                while($rows=mysqli_fetch_assoc($result))
                
                {
                ?>

               <?php 
                $date_Licensce = date_create("$rows[DriverLicensceDate]");
                $date_display_Licensce = date_format($date_Licensce , "d-F-Y"); 
                $date_DOB = date_create("$rows[DriverLicensceDate]");
                $date_display_DOB = date_format( $date_DOB , "d-F-Y");?>


               
                 <tr>
                    <td><?php echo $rows['ID'] ?></td>
                    <td><?php echo $rows['FirstName'] ?></td>
                    <td><?php echo $rows['LastName'] ?></td>
                    <td><?php echo $date_display_DOB ?></td>
                    <td><?php echo $rows['CountryOfBirth'] ?></td>
                    <td><?php echo $rows['DriverLicensce'] ?></td>
                    <td><?php echo $date_display_Licensce ?></td>
                    <td><img src="data:image/jpeg;base64,<?php echo base64_encode($rows['DL_Image']) ?>"></td>

                
                </tr>
                <?php 
                }
                
                ?>

            </table>
            <button onclick="homePage()">Add Customer</button>

        </div>
    </section>
    
</body>

</html>
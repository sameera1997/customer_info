function countriesDropdown() {


    fetch('https://restcountries.com/v2/all')
        .then((response) => {
            return response.json();
        })
        .then((countries) => {

            var select, option;

            select = document.getElementById("COB");

            for (var total = 0; total < countries.length; total++) {
                option = document.createElement('option');
                option.value = option.text = countries[total].name;
                select.add(option);

            }

        });
}

function homePage() {

    location.replace("http://localhost/dashboard/foster/home.html");
}

function displayPage() {

    location.replace("http://localhost/dashboard/foster/display.php");
}